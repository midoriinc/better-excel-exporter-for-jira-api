### What is this repository for? ###

This repository contains the sample post-function and service scripts for the JIRA Better Excel Plugin.
These allow automatic generation, saving and emailing Excel spreadsheets from JIRA data.

See: http://www.midori-global.com/products/jira-better-excel-plugin/documentation/api
