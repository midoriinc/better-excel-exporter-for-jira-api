import org.apache.log4j.Logger
import com.atlassian.jira.ComponentManager

// Better Excel Plugin for JIRA
// Groovy script to send a Excel spreadsheet rendered from a single issue, attached to an email.
// This is primarily meant to be used as a JIRA service with the Script Runner plugin.
// Note: if there are multiple "to" addresses, it generates the Excel file only once and sends the same file to each email address.
// See: http://www.midori-global.com/products/jira-better-excel-plugin/documentation/api

log = Logger.getLogger("SendIssueXlsService.groovy")

evaluate(new File("/path/to/my/services/IntegrationUtils.groovy"))

// Excel configuration
issueKey = "TEST-1"
templateName = "issue-navigator.xlsx"
title = "${issueKey} export"

// email configuration
to = "john.doe@acme-company.com" // can contain multiple addresses in a comma-separated list
subject = "${title} spreadsheet"
body = "Hi!\n\nPlease find the \"${title}\" spreadsheet in the attachment.\n\n(This email was sent by Better Excel Plugin for JIRA.)"


issue = ComponentManager.instance.issueManager.getIssueObject(issueKey)
xlsResult = integrationUtils.getXls(templateName, title, issue)
to.tokenize(",").each() {
	integrationUtils.sendFileInEmail(it.trim(), subject, body, xlsResult)
}

log.debug("Service completed")