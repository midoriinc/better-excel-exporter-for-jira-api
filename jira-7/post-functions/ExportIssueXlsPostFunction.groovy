import org.apache.log4j.Logger

// Better Excel Plugin for JIRA
// Groovy script to export a Excel spreadsheet rendered from a single transitioned issue, and to copy that to a filesystem path.
// This is primarily meant to be used as a JIRA workflow post-function with the Script Runner plugin.
// See: http://www.midori-global.com/products/jira-better-excel-plugin/documentation/api#workflows

log = Logger.getLogger("ExportIssueXlsPostFunction.groovy")

evaluate(new File("/path/to/my/post-functions/IntegrationUtils.groovy"))

transitionProperties = transientVars.descriptor.getAction(transientVars.actionId).metaAttributes

// Excel configuration
templateName = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "templateName", "issue-navigator.xlsx", binding.variables)
title = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "title", "${issue.key} export", binding.variables)

// file configuration
// (default path will include a timestamp suffix like "/tmp/TEST-123-20140425-085217.xlsx", in order to avoid overwriting a previous export)
path = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "path", "/tmp/${issue.key}-${new Date().format('yyyyMMdd-HHmmss')}.xlsx", binding.variables)


xlsResult = integrationUtils.getXls(templateName, title, issue)
integrationUtils.copyFileToPath(xlsResult, path)

log.debug("Post function completed")