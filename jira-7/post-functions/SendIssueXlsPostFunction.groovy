import org.apache.log4j.Logger

// Better Excel Plugin for JIRA
// Groovy script to send a Excel spreadsheet rendered from a single transitioned issue, attached to an email.
// This is primarily meant to be used as a JIRA workflow post-function with the Script Runner plugin.
// Note: if there are multiple "to" addresses, it generates the Excel file only once and sends the same file to each email address.
// See: http://www.midori-global.com/products/jira-better-excel-plugin/documentation/api#workflows

log = Logger.getLogger("SendIssueXlsPostFunction.groovy")

evaluate(new File("/path/to/my/post-functions/IntegrationUtils.groovy"))

transitionProperties = transientVars.descriptor.getAction(transientVars.actionId).metaAttributes

// Excel configuration
templateName = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "templateName", "issue-navigator.xlsx", binding.variables)
title = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "title", "${issue.key} export", binding.variables)

// email configuration
to = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "to", "john.doe@acme-company.com", binding.variables) // can contain multiple addresses in a comma-separated list
subject = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "subject", "${title} spreadsheet", binding.variables)
body = integrationUtils.getWorkflowTransitionProperty(transitionProperties, "body", "Hi!\n\nPlease find the \"${title}\" spreadsheet in the attachment.\n\n(This email was sent by Better Excel Plugin for JIRA.)", binding.variables)


xlsResult = integrationUtils.getXls(templateName, title, issue)
to.tokenize(",").each() {
	integrationUtils.sendFileInEmail(it.trim(), subject, body, xlsResult)
}

log.debug("Post function completed")